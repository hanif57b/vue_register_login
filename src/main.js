import Vue from 'vue'
import App from './App.vue'
import axios from 'axios'
import VueAxios from 'vue-axios'
import { router } from './router'
import { BootstrapVue, IconsPlugin } from 'bootstrap-vue'
import 'bootstrap/dist/css/bootstrap.css'
import 'bootstrap-vue/dist/bootstrap-vue.css'
import CxltToastr from 'cxlt-vue2-toastr'
import 'cxlt-vue2-toastr/dist/css/cxlt-vue2-toastr.css'
//import $ from 'jquery'
import { ValidationProvider } from 'vee-validate/dist/vee-validate.full.esm';
import { ValidationObserver } from 'vee-validate'

import VueLazyload from 'vue-lazyload'

Vue.use(VueLazyload)

import VueUploadMultipleImage from 'vue-upload-multiple-image'

export default {
    components: {
        VueUploadMultipleImage,
    },
}

import { HasError, AlertError } from 'vform'
Vue.config.devtools = true;

Vue.component("ValidationProvider", ValidationProvider)
Vue.component("ValidationObserver", ValidationObserver)

Vue.component(HasError.name, HasError)
Vue.component(AlertError.name, AlertError)

Vue.config.productionTip = false

Vue.use(BootstrapVue)
Vue.use(IconsPlugin)
Vue.use(CxltToastr, toastrConfigs)

Vue.prototype.$image_path = 'http://localhost:8000/';

var toastrConfigs = {
    position: 'top right',
    showDuration: 2000
}



Vue.use(VueAxios, axios)
new Vue({
    render: h => h(App),
    router
}).$mount('#app')