import Vue from 'vue'
import VueRouter from 'vue-router'


const routes = [{
        path: '/number',
        name: 'number',
        meta: {
            hideNavigation: false,
            hideHeader: true
        },
        component: () =>
            import ('./components/Number.vue')
    },
    {
        path: '/navbar',
        name: 'navbar',
        component: () =>
            import ('./components/Navbar.vue')
    },
    {
        path: '/register',
        name: 'register',
        meta: {
            hideNavigation: true,
            hideHeader: false
        },
        component: () =>
            import ('./components/Register.vue')
    },
    {
        path: '/login',
        name: 'login',
        meta: {
            hideNavigation: true,
            hideHeader: false
        },
        component: () =>
            import ('./components/Login.vue')
    },
    {
        path: '/',
        name: 'veevalidation',
        meta: {
            hideNavigation: true,
            hideHeader: false
        },
        component: () =>
            import ('./components/VeeValidationForm.vue')
    },
    {
        path: '/fileshow',
        name: 'fileshow',
        meta: {
            hideNavigation: false,
            hideHeader: true
        },
        component: () =>
            import ('./components/pages/FilesView.vue')
    },
    {
        path: '/mi',
        name: 'mi',
        meta: {
            hideNavigation: false,
            hideHeader: true
        },
        component: () =>
            import ('./components/pages/MI.vue')
    },
    {
        path: '/createblog',
        name: 'createblog',
        meta: {
            hideNavigation: false,
            hideHeader: true
        },
        component: () =>
            import ('./components/crud/CreateBlog')
    },
    {
        path: '/bloglist',
        name: 'bloglist',
        meta: {
            hideNavigation: false,
            hideHeader: true
        },
        component: () =>
            import ('./components/crud/BlogList')
    },
    {
        path: '/editblog/:id',
        name: 'editblog',
        meta: {
            hideNavigation: false,
            hideHeader: true
        },
        component: () =>
            import ('./components/crud/EditBlog')
    },
    {
        path: '/lheader',
        name: 'lheader',
        component: () =>
            import ('./components/Lheader')
    },
    {
        path: '/ccreate',
        name: 'ccreate',
        meta: {
            hideNavigation: true
        },
        component: () =>
            import ('./components/location/CountryCreate.vue')
    },
    {
        path: '/clist',
        name: 'clist',
        meta: {
            hideNavigation: true
        },
        component: () =>
            import ('./components/location/CountryList.vue')
    },
    {
        path: '/cedit/:id',
        name: 'cedit',
        meta: {
            hideNavigation: true
        },
        component: () =>
            import ('./components/location/EditCountry.vue')
    },

    {
        path: '/dcreate',
        name: 'dcreate',
        meta: {
            hideNavigation: true
        },
        component: () =>
            import ('./components/location/DivisionCreate.vue')
    },
    {
        path: '/discreate',
        name: 'discreate',
        meta: {
            hideNavigation: true
        },
        component: () =>
            import ('./components/location/DistrictCreate.vue')
    },
    {
        path: '/thanacreate',
        name: 'thanacreate',
        meta: {
            hideNavigation: true
        },
        component: () =>
            import ('./components/location/ThanaCreate.vue')
    },
]


Vue.use(VueRouter)
export const router = new VueRouter({
    routes,
    mode: 'history'
})